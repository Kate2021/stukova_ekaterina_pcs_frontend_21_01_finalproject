import React from 'react'
import Categories from "../components/Categories";
import SortPopup from "../components/SortPopup";
import PizzaBlock from "../components/PizzaBlock";
import {useDispatch, useSelector} from "react-redux";
import {setCategory} from '../redux/actions/filters';
import {fetchPizzas, setPizzas} from "../redux/actions/pizzas";
import {item} from "prompts/lib/util/style";

const categoryNames = ['Мясные', 'Вегетарианские', 'Гриль', 'Острые', 'Закрытые'];
const sortItems = [
    {name: 'популярности', type: 'popular'},
    {name: 'цене', type: 'price'},
    {name: 'алфавиту', type: 'alphabet'},
];


function selectFromDBWithCategory(category, dispatch) {
    fetch('http://localhost:3000/db.json')
        .then((resp) => resp.json())
        .then(json => {

            let pizzas = json.pizzas;
            if (category !== null) {
                pizzas = pizzas.filter(item => (item?.['category'] === category
                ));
            }

            dispatch(setPizzas(pizzas));
        });
}

function Home() {
    const dispatch = useDispatch();
    const items = useSelector(({pizzas}) => pizzas.items);
    const { category, sortBy } = useSelector(({ filters }) => filters);
    const activeCategory = useSelector(({filters}) => filters.category);
    const cartItems = useSelector(({ cart }) => cart.items);

    const onSelectCategory = index => {
        dispatch(setCategory(index));
    }

    React.useEffect(() => {
        dispatch(fetchPizzas(sortBy, category));
        selectFromDBWithCategory(category, dispatch);

    }, [category, sortBy]);

    return (
        <div className="container">
            <div className="content__top">
                <Categories
                    onClickItem={onSelectCategory}
                    items={categoryNames}
                    activeCategory={activeCategory}
                />
                <SortPopup items={sortItems}
                />
            </div>
            <h2 className="content__title">Все пиццы</h2>
            <div className="content__items">
                {
                    items && items.map((obj) => (
                        <PizzaBlock
                            key={obj.id} {...obj}
                            addedCount={cartItems[obj.id] && cartItems[obj.id].items.length}
                        />))
                }
            </div>
        </div>
    )
}

export default Home