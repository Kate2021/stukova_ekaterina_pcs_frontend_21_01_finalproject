import React from 'react';

function Contacts() {
    return (
        <div className="container">
            <h1 className="page__header">Контакты</h1>
            <div className="page__info">
                К вашим услугам в доступны две точки
                Вы можете оформить заказ, выбрав подходящий для вас пункт самовывоза или оформить доставку.
            </div>
            <div className="content__items">
                <div className="contacts">
                    <div className="contacts__item contacts__item--header"> Вокзальная, 25</div>
                    <div className="contacts__item">8 (343) 227-05-77</div>
                    <div className="contacts__item">с 10 до 24 ежедневно</div>

                </div>
                <div className="contacts">
                    <div className="contacts__item contacts__item--header">Краснолесья, 97</div>
                    <div className="contacts__item">8 (343) 227-05-77</div>
                    <div className="contacts__item">С 10 до 24 ежедневно</div>

                </div>
                <div className="contacts">
                    <div className="contacts__item contacts__item--header">Переулок Широкий, д.6</div>
                    <div className="contacts__item">8 (343) 227-05-77</div>
                    <div className="contacts__item">с 10 до 24 ежедневно</div>

                </div>

            </div>
        </div>
    )
}

export default Contacts