import React, {useState} from 'react'
import Button from "../components/Button";

function Login() {
    const [login, setLogin] = useState('User');
    const [password, setPassword] = useState('');

    function handleForm(event) {
        event.preventDefault();
        console.log(login);
        localStorage.setItem('login', login);
        return undefined;
    }

    return (
        <div className="container">
            <h1 className="page__header">Вход </h1>
            <div className="page__info">
                <form id="login-form">
                    <div className="form-group">
                        <label htmlFor="floatingInput">Почта</label>
                        <input className="form-control" id="login" defaultValue={login}
                               onChange={(e) => setLogin(e.target.value)} placeholder="User"/>

                    </div>
                    <div className="form-group">
                        <label htmlFor="floatingPassword">Пароль</label>
                        <input type="password" className="form-control" defaultValue={password} id="password"
                               onChange={(e) => setPassword(e.target.value)}
                               placeholder="Password"/>

                    </div>
                    <Button onClick={(e) =>handleForm(e)}>Войти</Button>
                </form>

            </div>
        </div>
    )
}

export default Login