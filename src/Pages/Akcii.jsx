import React from 'react';
import ActionBlock from "../components/ActionBlock";

function Akcii() {
    const [items, setItems] = React.useState([]);

    React.useEffect(() => {
        fetch('http://localhost:3000/action.json')
            .then((resp) => resp.json())
            .then(json => {
                setItems(json.sale);
            });
    }, []);


    return (
        <div className="container">
            <h1>Акции</h1>
            <div className="content__items">
                {items.map( (obj) => <ActionBlock key={obj.header} {...obj} />)}
            </div>
        </div>
    )
}

export default Akcii;