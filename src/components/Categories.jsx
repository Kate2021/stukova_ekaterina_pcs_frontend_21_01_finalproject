import React from 'react'


export default function Categories({items, onClickItem, activeCategory}) {
    const [activeItem, setActiveItem] = React.useState(null);

const onSelectItem = (index) => {
    setActiveItem(index);
    onClickItem(index);
};

    return (
            <div className="categories">
                <ul>
                    <li
                        //className="active"
                        className={activeCategory === null ? 'active' : ''}
                        onClick={() => onSelectItem (null)}
                    >Все</li>
                    {items.map((name, index) => (
                        <li
                            className={activeItem === index ? 'active' : ''}
                            onClick={() => onSelectItem (index)}
                            key={`${name}_${index}`}>
                            {name}
                        </li>
                        ))}
                </ul>
            </div>
    )
};