import React from "react";
function ActionBlock({header, image, content}){
    return (
        <div className="action">
            <img className="action__image"
                 src={image}/>
            <h3 className="action__title">{header}</h3>
            <div className="action__description"><p>{content}</p>
            </div>
        </div>
    )
}
export default ActionBlock