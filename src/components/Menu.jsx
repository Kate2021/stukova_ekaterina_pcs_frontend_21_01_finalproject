import React from 'react';
import Button from "./Button";
import {Link, useMatch, useResolvedPath} from "react-router-dom";

function Menu() {
    return (
        <div className="container">
            <CustomLink to="/akcii">Акции</CustomLink>
            <CustomLink to="/dostavka">Доставка</CustomLink>
            <CustomLink to="/secret">Секрет вкусной пиццы</CustomLink>
            <CustomLink to="/contacts">Контакты</CustomLink>
        </div>
    )
}

function CustomLink({children, to, ...props}) {
    let resolved = useResolvedPath(to);
    let match = useMatch({path: resolved.pathname, end: true});

    return (
        <Link
            style={{ cursor: match ? "default" : "" }}
            to={to}
            {...props}
        >
            <Button className={match ? "button--active-menu" : "button--outline"}>
                {children}
            </Button>
        </Link>

    );
}

export default Menu