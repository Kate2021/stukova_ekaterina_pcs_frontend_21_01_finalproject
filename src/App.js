import React from 'react';
import Header from './components/Header.jsx';
import Home from "./Pages/Home";
import Cart from "./Pages/Cart";
import {Route, Routes} from "react-router-dom";
import Menu from "./components/Menu";
import Akcii from "./Pages/Akcii";
import Dostavka from "./Pages/Dostavka";
import Contacts from "./Pages/Contacts";
import Secret from "./Pages/Secret";
import {useDispatch} from "react-redux";
import {setPizzas} from "./redux/actions/pizzas";
import NotFound from "./Pages/NotFound";
import Login from "./Pages/Login";
import RequiredAuth from "./fakeAuth/RequiredAuth";

function App() {
    const dispatch = useDispatch();


    React.useEffect(() => {
        fetch('http://localhost:3000/db.json')
            .then((resp) => resp.json())
            .then(json => {
                dispatch(setPizzas(json.pizzas));
            });
    }, []);


    return (
        <div className="wrapper">
            <Header/>
            <Menu/>
            <div className="content">
                <Routes>
                    <Route path="/" element={<RequiredAuth><Home/></RequiredAuth>} exact={true}/>
                    <Route path="/cart" element={<RequiredAuth><Cart/></RequiredAuth>} exact={true}/>
                    <Route path="/akcii" element={<RequiredAuth><Akcii/></RequiredAuth>} exact={true}/>
                    <Route path="/dostavka" element={<RequiredAuth><Dostavka/></RequiredAuth>} exact={true}/>
                    <Route path="/contacts" element={<RequiredAuth><Contacts/></RequiredAuth>} exact={true}/>
                    <Route path="/secret" element={<RequiredAuth><Secret/></RequiredAuth>} exact={true}/>
                    <Route path="*" element={<NotFound/>} />
                    <Route path="/login" element={<Login/>} />


                </Routes>
            </div>
        </div>

    );
}



export default App;
