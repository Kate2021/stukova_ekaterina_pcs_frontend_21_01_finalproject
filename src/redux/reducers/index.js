import filters from "./filters";
import pizzas from "./pizzas";

import {combineReducers} from "redux";
import cart from "./cart";

const rootReducer = combineReducers({
   filters: filters,
    pizzas: pizzas,
    cart: cart
});
export default rootReducer